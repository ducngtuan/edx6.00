import matplotlib as mpl
import matplotlib.pyplot as plt

def read_data(filename):
    with open(filename) as f:
        lines = [line.split() for line in f.readlines()]
        fields = [field for field in lines
                  if len(field) >= 3 and field[0].isdigit()]

        hi_temps = [int(field[1]) for field in fields]
        lo_temps = [int(field[2]) for field in fields]
        di_temps = [high - low for high, low in zip(hi_temps, lo_temps)]
        return hi_temps, lo_temps, di_temps

hi_temps, lo_temps, di_temps = read_data('julyTemps.txt')

mpl.rcParams['font.family'] = 'Calibri'
plt.plot(range(1, 32), di_temps, 'bo-')
plt.title('Day by Day Ranges in Temperature in Boston in July 2012')
plt.xlabel('Days')
plt.ylabel('Temperature Ranges')
plt.show()