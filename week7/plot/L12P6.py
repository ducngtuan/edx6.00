import matplotlib as mpl
import matplotlib.pyplot as plt

def read_data(filename):
    with open(filename) as f:
        lines = [line.split() for line in f.readlines()]
        fields = [field for field in lines
                  if len(field) >= 3 and field[0].isdigit()]

        hi_temps = [int(field[1]) for field in fields]
        lo_temps = [int(field[2]) for field in fields]
        di_temps = [high - low for high, low in zip(hi_temps, lo_temps)]
        return hi_temps, lo_temps, di_temps

hi_temps, lo_temps, di_temps = read_data('julyTemps.txt')

# Draw graph
x = range(1, 32)
plt.figure(figsize=(12, 5))
plt.plot(x, hi_temps, 'ro-', label='Daily highest')
plt.plot(x, lo_temps, 'bo-', label='Daily lowest')
plt.fill_between(x, hi_temps, lo_temps, color='red', alpha=0.3)
plt.fill_between(x, lo_temps, 0, color='blue', alpha=0.3)

# Draw title and legends
mpl.rcParams['font.family'] = 'Calibri'
plt.title('Day by Day Temperature in Boston in July 2012')
plt.xlim(1, 31)
plt.ylim(ymin=60)
plt.yticks(range(60, 100, 10))
plt.xlabel('Days')
plt.ylabel('Temperature')
plt.legend(loc='best')

# Draw grid
plt.grid(axis='y')
plt.vlines(x, 0, hi_temps, alpha=0.1)

plt.show()