from collections import defaultdict
import matplotlib as mpl
import matplotlib.pyplot as plt

def read_words(filename):
    with open(filename) as f:
        words = {line.strip() for line in f}
        return words

words = read_words("words.txt")
wlens = defaultdict(lambda: (0, 0))
for word in words:
    vowel_count = sum(1 for ch in word if ch in 'AEIOUaeiou')
    num_of_words, total_word_len = wlens[vowel_count]
    wlens[vowel_count] = (num_of_words + 1, total_word_len + len(word))

num_of_vowels = list(wlens.keys())
avg_len_of_words = [v[1]*1.0/v[0] for v in wlens.values()]
plt.plot(num_of_vowels, avg_len_of_words, 'yo-')

# Draw title and legends
mpl.rcParams['font.family'] = 'Calibri'
plt.title('Average length of words have the same number of vowels')
plt.xlabel('Number of vowels')
plt.ylabel('Average length')
plt.ylim(4, 8.5)

# Draw grid
plt.grid(axis='y')

plt.show()