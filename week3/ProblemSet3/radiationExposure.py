def f(x):
    import math
    return 10*math.e**(math.log(0.5)/5.27 * x)

def radiationExposure(start, stop, step):
    '''
    >>> radiationExposure(0, 5, 1)
    39.10318784326239
    >>> radiationExposure(5, 11, 1)
    22.94241041057671
    >>> radiationExposure(0, 11, 1)
    62.0455982538
    >>> radiationExposure(40, 100, 1.5)
    0.434612356115
    '''
    result = 0.0
    while start < stop:
        result += f(start) * step
        start += step
    return result

if __name__ == '__main__':
    import doctest
    doctest.testmod()