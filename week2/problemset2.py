def calculate_payment(balance, annualInterestRate, monthlyPaymentRate):
    monthly_interest_rate = annualInterestRate / 12.0
    begin_balance = balance

    for month in range(1, 13):
        minimum_monthy_payment = monthlyPaymentRate * balance
        monthly_unpaid_balance = balance - minimum_monthy_payment
        balance = monthly_unpaid_balance * (1 + monthly_interest_rate)

        print 'Month:', month
        print 'Minimum monthly payment:', round(minimum_monthy_payment, 2)
        print 'Remaining balance:', round(balance, 2)

    print 'Total paid:', round(begin_balance - balance, 2)
    print 'Remaining balance:', round(balance, 2)

def remain(balance, monthly_interest_rate, monthly_payment):
    for month in range(12):
        monthly_unpaid_balance = balance - monthly_payment
        balance = monthly_unpaid_balance * (1 + monthly_interest_rate)
    return balance

def lowest_payment(balance, annualInterestRate):
    monthly_interest_rate = annualInterestRate / 12.0
    monthly_payment = 10
    while remain(balance, monthly_interest_rate, monthly_payment) > 0:
        monthly_payment += 10
    
    print 'Lowest Payment:', monthly_payment

def lowest_payment2(balance, annualInterestRate):
    monthly_interest_rate = annualInterestRate / 12.0
    monthly_payment_lo = balance / 12
    monthly_payment_hi = balance * (1 + monthly_interest_rate) ** 12 / 12.0

    while monthly_payment_hi - monthly_payment_lo > 0.01:
        monthly_payment = (monthly_payment_lo + monthly_payment_hi) / 2.0
        
        remain_balance = remain(balance, monthly_interest_rate, monthly_payment)
        if  remain_balance > 0:
            monthly_payment_lo = monthly_payment
        else:
            monthly_payment_hi = monthly_payment

    print 'Lowest Payment:', round(monthly_payment, 2)

if __name__ == '__main__':
    # calculate_payment(4213, 0.2, 0.04)
    # lowest_payment(3329, 0.2) # 310
    # lowest_payment(4773, 0.2) # 440
    # lowest_payment(3926, 0.2) # 360
    lowest_payment2(320000, 0.2) # 29157.09
    lowest_payment2(999999, 0.18) # 90325.03