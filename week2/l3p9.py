print 'Please think of a number between 0 and 100!'

lo, hi = 0, 100
while lo <= hi:
    guess = (lo + hi) // 2

    print 'Is your secret number %d?' % guess
    response = raw_input("Enter 'h' to indicate the guess is too high."
                         " Enter 'l' to indicate the guess is too low."
                         " Enter 'c' to indicate I guessed correctly. ")
    if response == "l":
        lo = guess
    elif response == "h":
        hi = guess
    elif response == "c":
        print 'Game over. Your secret number was:', guess
        break
    else:
        print 'Sorry, I did not understand your input.'