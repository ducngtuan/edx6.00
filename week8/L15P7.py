import random
import itertools
import matplotlib.pyplot as plt

def roll_dice(n=3, sides=6):
    return [random.randint(1, sides) for _ in range(n)]

def is_all_sixes(roll):
    return all(x == 6 for x in roll)

def count_rolls():
    count = 0
    while count < 2000:
        count += 1
        if is_all_sixes(roll_dice()):
            break
    return count

def run_simulations(numTrials=1000):
    trials = []
    for i in range(numTrials):
        trials.append(count_rolls())

    mean = sum(trials) / numTrials
    stddev = (sum((x - mean)**2 for x in trials)*1.0 / numTrials)**0.5
    return trials, mean, stddev

numTrials = 2000
trials, mean, stddev = run_simulations(numTrials)

plt.text(800, 50, 'Number of simulations = {}\nMean = {}\nStdDev = {}'.format(numTrials, mean, stddev))
plt.title('Histogram of how many rolls we need until we get all sixes with 3 dices')
plt.xlabel('Number of needed rolls for getting all sixes')
plt.ylabel('Frequency')
plt.hist(trials, bins=100, range=(0, 1200))
plt.show()