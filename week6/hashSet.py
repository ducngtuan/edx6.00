class hashSet(object):
    def __init__(self, numBuckets):
        if not isinstance(numBuckets, int) or numBuckets <= 0:
            raise ValueError
        self.numBuckets = numBuckets
        self.buckets = [[] for i in range(numBuckets)]

    def hashValue(self, e):
        if not isinstance(e, int):
            raise ValueError
        return e % self.numBuckets

    def member(self, e):
        return e in self.buckets[self.hashValue(e)]

    def insert(self, e):
        if not self.member(e):
            self.buckets[self.hashValue(e)].append(e)

    def remove(self, e):
        try:
            self.buckets[self.hashValue(e)].remove(e)
        except:
            raise ValueError

    def getNumBuckets(self):
        return self.numBuckets
        
    def __str__(self):
        return '{' + str(self.buckets) + '}'