# 6.00 Problem Set 9

import numpy
import random
import pylab
from ps8b import *

#
# PROBLEM 1
#
def simulationDelayedTreatment(numTrials, delays, numViruses=100,
    maxPop=1000, maxBirthProb=0.1, clearProb=0.05,
    resistances={'guttagonol': False}, mutProb=0.005):
    """
    Runs simulations and make histograms for problem 1.

    Runs numTrials simulations to show the relationship between delayed
    treatment and patient outcome using a histogram.

    Histograms of final total virus populations are displayed for delays of 300,
    150, 75, 0 timesteps (followed by an additional 150 timesteps of
    simulation).

    numTrials: number of simulation runs to execute (an integer)
    """
    def simulationDelay(delay):
        vspop = [0] * numTrials
        for i in range(numTrials):
            vs = [ResistantVirus(maxBirthProb, clearProb, resistances, mutProb)
                  for _ in range(numViruses)]
            patient = TreatedPatient(vs, maxPop)
            for _ in range(delay):
                patient.update()
            patient.addPrescription('guttagonol')
            for _ in range(150):
                patient.update()
            vspop[i] = patient.getTotalPop()

        return vspop

    for index, delay in enumerate(delays):
        print('Run simulation for delay ' + str(delay))
        pylab.subplot(2, 2, index + 1, title="Delay = " + str(delay))
        vspop = simulationDelay(delay)
        cured = sum(1 for v in vspop if v <= 50) * 100.0 / len(vspop)
        print('{} cured...'.format(cured))
        pylab.hist(vspop)
    print('Done! Show graph...')
    pylab.show()

# simulationDelayedTreatment(150, [75])
# simulationDelayedTreatment(50, [150], numViruses=50)
# simulationDelayedTreatment(50, [150], numViruses=75)
# simulationDelayedTreatment(50, [150], numViruses=100)




#
# PROBLEM 2
#
def simulationTwoDrugsDelayedTreatment(numTrials, delays, numViruses=100,
    maxPop=1000, maxBirthProb=0.1, clearProb=0.05,
    resistances={'guttagonol': False, 'grimpex': False}, mutProb=0.005):
    """
    Runs simulations and make histograms for problem 2.

    Runs numTrials simulations to show the relationship between administration
    of multiple drugs and patient outcome.

    Histograms of final total virus populations are displayed for lag times of
    300, 150, 75, 0 timesteps between adding drugs (followed by an additional
    150 timesteps of simulation).

    numTrials: number of simulation runs to execute (an integer)
    """
    def simulationDelay(delay):
        vspop = [0] * numTrials
        for i in range(numTrials):
            vs = [ResistantVirus(maxBirthProb, clearProb, resistances, mutProb)
                  for _ in range(numViruses)]
            patient = TreatedPatient(vs, maxPop)
            for _ in range(150):
                patient.update()
            patient.addPrescription('guttagonol')
            for _ in range(delay):
                patient.update()
            patient.addPrescription('grimpex')
            for _ in range(150):
                patient.update()
            vspop[i] = patient.getTotalPop()

        return vspop

    for index, delay in enumerate(delays):
        print('Run simulation for delay ' + str(delay))
        pylab.subplot(2, 2, index + 1, title="Delay = " + str(delay))
        vspop = simulationDelay(delay)
        cured = sum(1 for v in vspop if v <= 50) * 100.0 / len(vspop)
        print('{} cured...'.format(cured))
        pylab.hist(vspop)
    print('Done! Show graph...')
    pylab.show()
# simulationTwoDrugsDelayedTreatment(125, [0, 75, 150, 300])
